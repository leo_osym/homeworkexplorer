﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Explotter
{
    public partial class MainForm : Form
    {
        const int WM_DEVICECHANGE = 0x219;
        const int DBT_DEVICEARRIVAL = 0x8000;
        const int DBT_DEVICEREMOVECOMPLETE = 0x8004;

        List<string> myPath = new List<string>();
        private string[] view = { "Details", "Large icons", "List", "Small icons", "Tile" };

        public MainForm()
        {
            InitializeComponent();
            toolStripComboBoxSelectView.Items.AddRange(view);
            toolStripComboBoxSelectView.SelectedItem = view[0];
            driveListView.View = getView();
        }

        protected override void WndProc(ref Message m)
        {
            if(m.Msg==WM_DEVICECHANGE
                && (int)m.WParam == DBT_DEVICEARRIVAL
                || (int)m.WParam == DBT_DEVICEREMOVECOMPLETE)
            {
                FillDriveButtons();
            }
                base.WndProc(ref m);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {   
            FillDriveButtons();
            FillList(Directory.GetLogicalDrives()[0]);
            driveListView.MouseDoubleClick += driveListView_ItemClick;
            myPath.Add(Directory.GetLogicalDrives()[0]);

            FillProcess();
            processListView.MouseDoubleClick += processListView_ItemClick;
            timerProcesses.Start();
        }

        #region DrivesView
        private void driveListView_ItemClick(object sender, MouseEventArgs e)
        {
            if (driveListView.SelectedIndices.Count == 1) 
            {
                myPath.Add("/" + driveListView.SelectedItems[0].Text);
                FileAttributes attr = File.GetAttributes(string.Concat(myPath.ToArray()));
                if (attr.HasFlag(FileAttributes.Directory))
                    FillList(string.Concat(myPath.ToArray()));
                else
                {
                    // redirect process to system app
                    System.Diagnostics.Process.Start(string.Concat(myPath.ToArray()));
                    myPath.RemoveAt(myPath.Count - 1);
                }
            }
        }

        private void FillDriveButtons()
        {
            toolStrip1.Items.Clear();
            ToolStripButton btnUndo = new ToolStripButton
            {
                Image = Image.FromFile("arrow_undo.png"),
                Text = "Back",
                DisplayStyle = ToolStripItemDisplayStyle.ImageAndText
            };
            toolStrip1.Items.Add(btnUndo);
            btnUndo.Click += (s, a) => {
                if(myPath.Count>1)
                    myPath.RemoveAt(myPath.Count - 1);
                FillList(string.Concat(myPath.ToArray()));
            };

            foreach (string drive in Directory.GetLogicalDrives())
            {
                ToolStripButton btn = new ToolStripButton
                {
                    Image = IconHelper.ExtractAssociatedIcon(drive).ToBitmap(),
                    Text = drive.Substring(0,2),
                    Tag=drive,
                    DisplayStyle = ToolStripItemDisplayStyle.ImageAndText
                };
                toolStrip1.Items.Add(btn);
                btn.Click += (s, a) => {
                    myPath.Clear();
                    myPath.Add((s as ToolStripButton).Tag as String);
                    FillList(string.Concat(myPath.ToArray()));
                };
                         
            }
        }

        /// <summary>
        /// Catching some errors while opening drive
        /// </summary>
        /// <param name="drive"></param>
        private void FillList(string drive)
        {
            driveListView.Items.Clear();
            DirectoryInfo dir = new DirectoryInfo(drive);
            imageListSmall.Images.Clear();

            try
            {
                FillDirectory(dir);
            }
            catch (System.IO.IOException ex)
            {
                myPath.RemoveAt(myPath.Count - 1);
                FillList(string.Concat(myPath.ToArray()));
                MessageBox.Show($"Error while opening drive:\n{ex.Message}");
            }
            catch (System.UnauthorizedAccessException ex)
            {
                myPath.RemoveAt(myPath.Count - 1);
                FillList(string.Concat(myPath.ToArray()));
                MessageBox.Show($"Access error:\n{ex.Message}");
            }
        }

        /// <summary>
        /// Fill view with inner directories and files
        /// </summary>
        /// <param name="dir">Root directory</param>
        private void FillDirectory(DirectoryInfo dir)
        {
            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                imageListSmall.Images.Add(di.Name, IconHelper.ExtractAssociatedIcon(Path.GetFullPath(di.FullName)).ToBitmap());
                imageListLarge.Images.Add(di.Name, IconHelper.ExtractAssociatedIcon(Path.GetFullPath(di.FullName)).ToBitmap());

                ListViewItem item = new ListViewItem { Text = di.Name, ImageKey = di.Name };
                item.SubItems.Add(di.CreationTime.ToString());
                item.SubItems.Add("");
                item.SubItems.Add(di.Attributes.ToString());

                driveListView.Items.Add(item);

            }
            foreach (FileInfo fi in dir.GetFiles())
            {
                imageListSmall.Images.Add(fi.Name, Icon.ExtractAssociatedIcon(fi.FullName).ToBitmap());
                imageListLarge.Images.Add(fi.Name, Icon.ExtractAssociatedIcon(fi.FullName).ToBitmap());

                ListViewItem item = new ListViewItem { Text = fi.Name, ImageKey = fi.Name };
                item.SubItems.Add(fi.CreationTime.ToString());
                item.SubItems.Add(String.Format("{0:0.00} MB", (fi.Length / (1024.0 * 1024.0))));
                item.SubItems.Add(fi.Attributes.ToString());

                driveListView.Items.Add(item);
            }
        }
        #endregion

        #region ProcessView
        private void FillProcess()
        {
            processListView.Items.Clear();
            ListView temp = new ListView();
            var aps = Process.GetProcesses();
            foreach (var ap in aps)
            {
                ListViewItem item = new ListViewItem { Text = ap.ProcessName, ImageKey = ap.ProcessName };
                item.SubItems.Add(ap.Id.ToString());
                item.SubItems.Add(ap.Responding == true ? "Responding" : "Not Responding");
                item.SubItems.Add((ap.WorkingSet64 / 1024).ToString() + " KBytes");
                processListView.Items.Add(item);
            }

        }

        private void processListView_ItemClick(object sender, MouseEventArgs e)
        {
            if (processListView.SelectedItems.Count == 1)
            {
                Process[] process = Process.GetProcessesByName(processListView.SelectedItems.ToString());
                if (MessageBox.Show("Do you really want to close this process?", "Close process",
                    MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    process[0].Kill();
                }
                FillProcess();
            }
        }

        private void timerProcesses_Tick(object sender, EventArgs e)
        {
            FillProcess();
        }
        #endregion

        #region ChooseView
        private View getView()
        {
            string str = toolStripComboBoxSelectView.SelectedItem.ToString();
            if (str.Equals(view[0])) return View.Details;
            if (str.Equals(view[1])) return View.LargeIcon;
            if (str.Equals(view[2])) return View.List;
            if (str.Equals(view[3])) return View.SmallIcon;
            else return View.Tile;
        }

        private void toolStripComboBoxSelectView_DropDownClosed(object sender, EventArgs e)
        {
            driveListView.View = getView();
        }
        #endregion

        #region NotWorkingCode
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // Actually, doesn't work yet
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.UseShellExecute = true;
            proc.WorkingDirectory = Environment.CurrentDirectory;
            proc.FileName = Application.ExecutablePath;
            proc.Verb = "runas";
            try
            {
                Process.Start(proc);
            }
            catch
            {
                return;
            }
            Application.Exit();  // Quit itself*/
        }
        #endregion


    }
}
